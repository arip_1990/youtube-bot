import time
import random
from threading import Thread, Event
from selenium.common.exceptions import WebDriverException, TimeoutException
from Parser import Parser
from Youtube import Youtube


proxy = ''
progress = 0
error = False


def print_status(total: int):
    loader = ['-', '\\', '|', '/']
    i = 0
    print('\033[1J\033[HПросмотр видео:')
    while not status_event.wait(0.2):
        print(f'\033[2K{loader[i]} Видео: {progress} из {total} -> {proxy}\r', end='')
        i += 1
        if i == 4:
            i = 0
    if error:
        print(f'\033[2K{loader[i]} Видео: {progress} из {total} -> {proxy} \033[1;31mError\033[0m')
    else:
        print(f'\033[2K{loader[i]} Видео: {progress} из {total} -> {proxy} \033[1;32mOk\033[0m')


if __name__ == '__main__':
    urls = ['https://youtu.be/4wxLp6xSXQU']
    status_event = Event()
    Thread(target=print_status, args=(1,)).start()
    parser = Parser()
    parser.parser_proxy()
    proxies = parser.get_proxies()

    youtube = None
    try:
        for url in urls:
            progress += 1
            time.sleep(random.randint(5, 10))
            while True:
                proxy = proxies.pop(0)
                youtube = Youtube(
                    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
                    False, proxy
                )
                try:
                    youtube.watch(url)
                    break
                except TimeoutException:
                    youtube.close()
                    if not len(proxies):
                        break
    except WebDriverException as e:
        error = True

    if youtube:
        youtube.close()
    status_event.set()

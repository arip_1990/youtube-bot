import requests
from bs4 import BeautifulSoup


class Parser:
    proxy_url = 'https://free-proxy-list.net/'
    proxies = []

    def parser_proxy(self):
        page = self.get_page(self.proxy_url)
        soup = BeautifulSoup(page, 'html.parser')
        for row in soup.select('table#proxylisttable  tbody > tr'):
            cols = row.select('td')
            if cols[4].string.strip() == 'elite proxy' and cols[6].string.strip() == 'yes':
                self.proxies.append(f'{cols[0].string}:{cols[1].string}')

    @staticmethod
    def get_page(url: str) -> str:
        response = requests.get(url)

        if response.status_code != 200:
            raise Exception('Error request')

        return response.text

    def get_proxies(self) -> list:
        return self.proxies

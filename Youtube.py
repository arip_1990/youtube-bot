import time
import pathlib
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class Youtube:
    def __init__(self, agent: str, headless: bool = False, proxy: str = None):
        desired = DesiredCapabilities.CHROME
        desired['loggingPrefs'] = {'performance': 'ALL'}

        options = webdriver.ChromeOptions()
        if headless:
            options.add_argument('--headless')
        options.add_argument('--window-size=1080,720')
        options.add_argument('--disable-xss-auditor')
        options.add_argument('--disable-web-security')
        options.add_argument('--allow-running-insecure-content')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-setuid-sandbox')
        options.add_argument('--disable-webgl')
        options.add_argument('--disable-popup-blocking')
        options.add_argument(f'--user-agent={agent}')
        options.add_argument('--load-extension=./stopper')
        if proxy:
            options.add_argument(f'--proxy-server={proxy}')

        chromedriver = str(pathlib.Path().absolute()) + '/chromedriver'
        self.driver = webdriver.Chrome(chromedriver, options=options, desired_capabilities=desired)
        self.driver.implicitly_wait(10)
        self.driver.set_page_load_timeout(20)

    def watch(self, url: str) -> None:
        self.driver.get(url)

        try:
            WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, '#player #container button.ytp-play-button'))
            )

            container = self.driver.find_element_by_css_selector('#player #container')
            auto_play = container.find_element_by_css_selector("button.ytp-button[data-tooltip-target-id='ytp-autonav-toggle-button']")
            auto = auto_play.find_element_by_css_selector('.ytp-autonav-toggle-button').get_attribute('aria-checked')
            if bool(auto):
                auto_play.click()

            time_player = container.find_element_by_css_selector('.ytp-time-display')
            total_time = time_player.find_element_by_css_selector('.ytp-time-duration').text
            while True:
                time.sleep(1)
                current_time = time_player.find_element_by_css_selector('.ytp-time-current').text
                if current_time == total_time:
                    break
        except WebDriverException as e:
            raise Exception(e)

    def close(self) -> None:
        self.driver.close()
